import react from 'react';
import { Route, Router, Switch } from 'react-router';
import './App.css';
import Navbar from './components/Navbar';
import Home from '../src/components/pages/Home'
import Products from './components/pages/Products';
import Services from './components/pages/Services';
import SignUp from './components/pages/SignUp';
import SignInOutContainer from './components/pages/SignInOutContainer';


function App() {
  document.title="TRVL"
  return (
    <>
  
    <Switch>
    <Route exact path="/">
    <Navbar/>
    <Home/>
    </Route>
    <Route  path="/products">
    <Products/>
    </Route>
    <Route  path="/services">
    <Services/>
    </Route>
    <Route  path="/sign-up">
    <SignInOutContainer/>
    </Route>
    </Switch>
    </>
  );
}

export default App;
