import React, { useEffect, useState } from 'react'
import axios from 'axios';
import "./SingleProducts.css"
import Checkbox from '../Checkbox.js';
import Search from "../pages/Search.js"

function SingleProducts(){
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(null)
    const [search, setSearch]=useState("");

   
   
    useEffect(()=>{
        setLoading(true)
        axios.get('https://fakestoreapi.com/products')
        .then(response =>{
            setData(response.data)  
            localStorage.setItem("products",JSON.stringify(response.data))
    })
    .catch(err => console.dir(err))
            .finally(() => setLoading(false))
    }, [])
  
    
    return (  
      <>
      <Search data={data} search={search} setSearch={setSearch} setData={setData}/>
      
      <section>
   
        {loading && "Loading..."}
        {!!data && data.length > 0 ? data.map((product) => {
            return(
        
              <div key={product.id} className="productsContainer">
                  <div className="productsCard">
                      <Checkbox/>
                   <img 
                   
                    src={product.image} 
                />
                 <h5> {product.title}</h5>
                 <p>price: {product.price}</p>
                 </div>
              </div>
            )   
          }):(<p>API did not provided any product, try again.</p>)
        }
      </section>
      </>
    )
}

export default SingleProducts;