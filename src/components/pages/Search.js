import React,{useEffect} from 'react';
import "./Search.css"

function Search({data,setData, search , setSearch}) {
    
  useEffect (()=>{
    const filtered = !search
    ? data
    : data.filter((product) =>
        product.title.toLowerCase().includes(search.toLowerCase())
      );
      setData(filtered)
     
  },[search])
     

        const handleSearchChange =(e)=>{
            setSearch(e.target.value);
          };

    return (

       
            <input type="text" placeholder="Search..." value={search} onChange={handleSearchChange} />
    )
}

export default Search
