import React from 'react'
import CardItem from './CardItem'
import './Cards.css';

function Cards() {
    return (
        <div className='cards'>
            <h1>Find out Georgia!</h1>
            <div className="cards__container"></div>
            <div className="cards__wrapper"></div>
            
            <ul className="cards__items">
                <CardItem 
                src='images/img-9.jpg'
                text='explore Georgia'
                label='Adventure'
                path='/services'
                />
                 <CardItem 
                src='images/img-2.jpg'
                text='Yazbegi'
                label='Moountain'
                path='/services'
                />
                  <CardItem 
                src='images/img-8.jpg'
                text='beautiful lake'
                label='lake'
                path='/services'
                />
            </ul>
            <ul className="cards__items">
                <CardItem 
                src='images/img-3.jpg'
                text='Racha'
                label='Adventure'
                path='/services'
                />
                 <CardItem 
                src='images/img-4.jpg'
                text='Tbilisi'
                label='Ancient City'
                path='/services'
                />
                  <CardItem 
                src='images/img-1.jpg'
                text='Gergeti Church'
                label='church'
                path='/services'
                />
            </ul>
            
        </div>
    )
}

export default Cards
