import React,{useState} from 'react';
import './Checkbox.css';
import {animated, useSpring,config, useChain, useSpringRef} from "react-spring";

export default function Checkbox() {
        const [isChecked, setIsChecked] = useState(false);
        const checkboxAnimationRef = useSpringRef();
        const checkboxAnimationStyle =useSpring({
            backgroundColor: isChecked ? "#808" : "#fff",
            borderColor: isChecked? "#808" :"#ddd",
            config:config.gentle,
            ref: checkboxAnimationRef
        });
   const [checkmarkLength, setCheckmarkLength] = useState(null)
   const checkmarkAnimationStyle = useSpring({
       x: isChecked ? 0 : checkmarkLength,
       config : config.gentle,
       ref: checkboxAnimationRef
   });

   useChain(
       isChecked 
       ? [checkboxAnimationRef, checkboxAnimationRef]
       : [checkboxAnimationRef,checkboxAnimationRef],
[0, 0.1]
   );

    return (
    
        <label>
      <input
        type="checkbox"
        onChange={() => {
          setIsChecked(!isChecked);
        }}
      />
      <animated.svg
      style ={checkboxAnimationStyle}
        className={`checkbox ${isChecked ? "checkbox--active" : ""}`}
        aria-hidden="true"
        viewBox=" 0 0 15 11"
        fill="none"
      >
          <animated.path
          d="M1 4.5L5 9L14 1"
          strokeWidth="2"
          stroke="#fff"
          strokeDasharray={checkmarkLength}
          strokeDashoffset ={checkmarkAnimationStyle.x}
        
          ref={(ref) => {
              if(ref){
                  setCheckmarkLength(ref.getTotalLength());
              }
          }}
          />
          </animated.svg>
      
    </label>
  );
}
